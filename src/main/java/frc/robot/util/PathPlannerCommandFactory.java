package frc.robot.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.stream.Stream;

import com.pathplanner.lib.PathConstraints;
import com.pathplanner.lib.PathPlanner;
import com.pathplanner.lib.auto.PIDConstants;
import com.pathplanner.lib.auto.SwerveAutoBuilder;

import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.DrivetrainSubsystem;

public class PathPlannerCommandFactory {
    private SwerveAutoBuilder m_autoBuilder;
    private HashMap<String, Command> m_events;
    private DrivetrainSubsystem m_swerve;
    private PathConstraints m_constraints = new PathConstraints(3.0, 2.0);
    

    public PathPlannerCommandFactory() {}

    public PathPlannerCommandFactory withEvents(HashMap<String, Command> events) {
        this.m_events = events;
        return this;
    }

    public PathPlannerCommandFactory withDrivetrain(DrivetrainSubsystem swerve) {
        this.m_swerve = swerve;
        return this;
    }

    public HashMap<String, Command> build() {
        if(m_autoBuilder == null) {
            m_autoBuilder = new SwerveAutoBuilder(
                m_swerve.getPose, 
                m_swerve.resetPose,
                m_swerve.m_kinematics,
                new PIDConstants(5.0, 0, 0), 
                new PIDConstants(1.5, 0, 0), 
                m_swerve.setModuleStates, 
                m_events, 
                true,
                m_swerve
            );
        }

        HashMap<String, Command> pp_commands = new HashMap<>();

        try {
            try(Stream<Path> paths = Files.walk(Path.of(Filesystem.getDeployDirectory()+"/pathplanner"))) {
                System.out.println("*****CREATING PATHS*****");
                paths.filter((path) -> Files.isRegularFile(path) && path.toString().endsWith(".path")).forEach((path) -> {
                    String filename = path.getFileName().toString().replaceFirst(".path$", "");
                    System.out.println("Created: " + filename);

                    PathConstraints constraints = PathPlanner.getConstraintsFromPath(filename);
                    pp_commands.put(
                        filename,
                        m_autoBuilder.fullAuto(
                            PathPlanner.loadPathGroup(
                                filename,
                                (constraints == null) ? m_constraints : constraints 
                            )
                        )
                    );
                });
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
        
        return pp_commands;
    }
}
