// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
  
  public static final class SwerveConstants {
    // TODO: Check measurements to ensure that they are correct
    /**
    * The left-to-right distance between the drivetrain wheels
    *
    * Should be measured from center to center.
    */
    public static final double DRIVETRAIN_TRACKWIDTH_METERS = 0.619125; // 24 3/8"
    /**
    * The front-to-back distance between the drivetrain wheels.
    *
    * Should be measured from center to center.
    */
    public static final double DRIVETRAIN_WHEELBASE_METERS = 0.8636; // 34"

    // TODO: Ensure all motor and encoder ids are correct
    // TODO: Find module offsets (this is good practice, even though it is very painful)
    public static final int BACK_RIGHT_MODULE_DRIVE_MOTOR = 9;
    public static final int BACK_RIGHT_MODULE_STEER_MOTOR = 4;
    public static final int BACK_RIGHT_MODULE_STEER_ENCODER = 10;
    public static final double BACK_RIGHT_MODULE_STEER_OFFSET = -Math.toRadians(65.478515625);//-Math.toRadians(66.7089-90);

    public static final int BACK_LEFT_MODULE_DRIVE_MOTOR = 1;
    public static final int BACK_LEFT_MODULE_STEER_MOTOR = 3;
    public static final int BACK_LEFT_MODULE_STEER_ENCODER = 12;
    public static final double BACK_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(340.83984375);//-Math.toRadians(76.222+180);

    public static final int FRONT_RIGHT_MODULE_DRIVE_MOTOR = 5;
    public static final int FRONT_RIGHT_MODULE_STEER_MOTOR = 6;
    public static final int FRONT_RIGHT_MODULE_STEER_ENCODER = 13;
    public static final double FRONT_RIGHT_MODULE_STEER_OFFSET = -Math.toRadians(64.07226525);//-Math.toRadians(68.818-90);

    public static final int FRONT_LEFT_MODULE_DRIVE_MOTOR = 8;
    public static final int FRONT_LEFT_MODULE_STEER_MOTOR = 7;
    public static final int FRONT_LEFT_MODULE_STEER_ENCODER = 11;
    public static final double FRONT_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(168.4863984);//-Math.toRadians(167.167-90);
  }

  public static final class ArmConstants {

    public static final int LEFT_SOLENOID_ID = 0;
    public static final int RIGHT_SOLENOID_ID = 1;
  }

  public static final class IntakeConstants {
    public static final int ROLLER_MOTOR_ID = 55;
    public static final double ROLLER_SPEED = 1.0;
  }

  public static final class ControllerConstants {
    public static final double TRIGGER_THRESH = 0.15;
  }

}
