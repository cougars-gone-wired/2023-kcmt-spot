// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import frc.robot.commands.AutonomousCommands.DoNothingCommand;

import java.util.HashMap;

import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.PrintCommand;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.commands.armCommands.LowerArmCommand;
import frc.robot.commands.armCommands.RaiseArmCommand;
import frc.robot.commands.driveCommands.BalanceCommand;
import frc.robot.commands.driveCommands.DefaultDriveCommand;
import frc.robot.commands.driveCommands.ZeroGyroCommand;
import frc.robot.commands.intakeCommands.IntakeCommand;
import frc.robot.commands.intakeCommands.OutakeCommand;
import frc.robot.commands.intakeCommands.RollSpeedCommand;
import frc.robot.subsystems.*;
import frc.robot.util.PathPlannerCommandFactory;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and trigger mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  private final DrivetrainSubsystem m_drivetrainSubsystem = new DrivetrainSubsystem();
  private final ArmSubsystem m_armSubsystem = new ArmSubsystem();
  private final IntakeSubsystem m_intakeSubsystem = new IntakeSubsystem();
  
  private final CommandXboxController m_driverController = new CommandXboxController(0);
  private final CommandXboxController m_manipulatorController = new CommandXboxController(1);
  private final DoNothingCommand doNothingCommand = new DoNothingCommand();
  private final SendableChooser<Command> autoChooser = new SendableChooser<>();

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    // Configure the trigger bindings
    configureBindings();
    configureAutoChooser();
  }


  public void configureAutoChooser() {
    autoChooser.addOption("Do Nothing", doNothingCommand);
    autoChooser.setDefaultOption("Do Nothing", doNothingCommand);
    SmartDashboard.putData("Auto Programs", autoChooser);

    // Set up events for the pathplanner autos and add pathplanner paths to the autoChooser
    
    // The events map contains all of the names of events used in paths, and the command associated with them
    HashMap<String, Command> events = new HashMap<>();

    // To add events to the map, the following structure is used:
    events.put("Example event key", new PrintCommand("Example event command"));
    // The <"Example event key"> should be replaced with the event names used in paths, and the <new PrintCommand> should be replaced with the command you want called

    events.put("Outtake", new OutakeCommand(m_intakeSubsystem).raceWith(new WaitCommand(1.0)));
    events.put("Intake", new IntakeCommand(m_intakeSubsystem).raceWith(new WaitCommand(1.0)));
    events.put("Lower Arm", new LowerArmCommand(m_armSubsystem));
    events.put("Raise Arm", new RaiseArmCommand(m_armSubsystem));
    events.put("Balance", new BalanceCommand(m_drivetrainSubsystem));
    
    // Get the path planner auto commands to add to the selector
    HashMap<String, Command> pp_commands = new PathPlannerCommandFactory()
      .withDrivetrain(m_drivetrainSubsystem)
      .withEvents(events)
      .build();

    // Add each of the commands to the autoChooser so we can select them as autos
    pp_commands.forEach((name, command) -> autoChooser.addOption(name, command));
  }
  /**
   * Use this method to define your trigger->command mappings. Triggers can be created via the
   * {@link Trigger#Trigger(java.util.function.BooleanSupplier)} constructor with an arbitrary
   * predicate, or via the named factories in {@link
   * edu.wpi.first.wpilibj2.command.button.CommandGenericHID}'s subclasses for {@link
   * CommandXboxController Xbox}/{@link edu.wpi.first.wpilibj2.command.button.CommandPS4Controller
   * PS4} controllers or {@link edu.wpi.first.wpilibj2.command.button.CommandJoystick Flight
   * joysticks}.
   */
  private void configureBindings() {
    m_drivetrainSubsystem.setDefaultCommand(
      new DefaultDriveCommand(
        m_drivetrainSubsystem, 
        () -> -modifyAxis(m_driverController.getLeftY()) * DrivetrainSubsystem.MAX_VELOCITY_METERS_PER_SECOND, 
        () -> -modifyAxis(m_driverController.getLeftX()) * DrivetrainSubsystem.MAX_VELOCITY_METERS_PER_SECOND, 
        () -> -modifyAxis(m_driverController.getRightX()) * DrivetrainSubsystem.MAX_ANGULAR_VELOCITY_RADIANS_PER_SECOND
    ));

    m_driverController.back().onTrue(new ZeroGyroCommand(m_drivetrainSubsystem));


    m_manipulatorController.b().onTrue(new RaiseArmCommand(m_armSubsystem));
    m_manipulatorController.a().onTrue(new LowerArmCommand(m_armSubsystem));
    m_manipulatorController.rightTrigger(Constants.ControllerConstants.TRIGGER_THRESH).whileTrue(new RollSpeedCommand(m_intakeSubsystem, m_manipulatorController::getRightTriggerAxis));
    m_manipulatorController.leftTrigger(Constants.ControllerConstants.TRIGGER_THRESH).whileTrue(new RollSpeedCommand(m_intakeSubsystem, () -> -m_manipulatorController.getLeftTriggerAxis()));

    m_manipulatorController.start().onTrue(new InstantCommand(m_armSubsystem::disableCompressor, m_armSubsystem));
    m_manipulatorController.back().onTrue(new InstantCommand(m_armSubsystem::enableCompressor, m_armSubsystem));
    

  }

  private static double deadband(double value, double deadband) {
    if(Math.abs(value) > deadband) {
      // Subtract deadband from the value so that when value = deadband, the output is zero
      value = (value - Math.copySign(deadband, value));

      // Re-normalize value by 1-deadband, b/c 1-deadband will be our max, so we want to output 1.0 when (value - deadband) = 1.0 - deadband
      return value / (1.0 - deadband);
    } else {
      return 0.0;
    }
  }

  private static double modifyAxis(double value) {
    // Apply a nice arbitrary deadband value
    value = deadband(value, 0.05);

    // Square the axis whilst maintaining the sign for more finite control
    value = Math.copySign(value * value, value);

    return value;
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An example command will be run in autonomous
    return autoChooser.getSelected();
  }
}
