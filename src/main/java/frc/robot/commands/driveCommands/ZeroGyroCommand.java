package frc.robot.commands.driveCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DrivetrainSubsystem;

public class ZeroGyroCommand extends CommandBase {
    private DrivetrainSubsystem m_drivetrainSubsystem;

    public ZeroGyroCommand(DrivetrainSubsystem drivetrainSubsystem) {
        m_drivetrainSubsystem = drivetrainSubsystem;

        // Dont require the subsystem b/c it will interrupt drive
    }

    @Override
    public void initialize() {
        m_drivetrainSubsystem.zeroGyroscope();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
