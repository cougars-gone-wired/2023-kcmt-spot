package frc.robot.commands.intakeCommands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IntakeSubsystem;

public class RollSpeedCommand extends CommandBase {
    private IntakeSubsystem m_intakeSubsystem;
    private DoubleSupplier m_speedSupplier;
    
    public RollSpeedCommand(IntakeSubsystem intakeSubsystem, DoubleSupplier speedSupplier) {
        m_intakeSubsystem = intakeSubsystem;
        m_speedSupplier = speedSupplier;

        addRequirements(m_intakeSubsystem);
    }

    @Override
    public void execute() {
        m_intakeSubsystem.rollSpeed(m_speedSupplier.getAsDouble());
    }

    @Override
    public void end(boolean interrupted) {
        m_intakeSubsystem.stopRoller();
    }
    
}
