package frc.robot.commands.intakeCommands;


import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IntakeSubsystem;

public class OutakeCommand extends CommandBase {
    private IntakeSubsystem m_intakeSubsystem;
    
    public OutakeCommand(IntakeSubsystem intakeSubsystem) {
        m_intakeSubsystem = intakeSubsystem;

        addRequirements(m_intakeSubsystem);
    }

    @Override
    public void execute() {
        m_intakeSubsystem.rollOut();
    }

    @Override
    public void end(boolean interrupted) {
        m_intakeSubsystem.stopRoller();
    }
}
