package frc.robot.commands.armCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmSubsystem;

public class LowerArmCommand extends CommandBase {
    private ArmSubsystem m_armSubsystem;

    public LowerArmCommand(ArmSubsystem armSubsystem) {
        m_armSubsystem = armSubsystem;

        addRequirements(m_armSubsystem);
    }

    @Override
    public void initialize() {
        m_armSubsystem.lowerArm();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
