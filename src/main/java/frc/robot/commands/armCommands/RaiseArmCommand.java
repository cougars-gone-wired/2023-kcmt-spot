package frc.robot.commands.armCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmSubsystem;

public class RaiseArmCommand extends CommandBase {
    private ArmSubsystem m_armSubsystem;

    public RaiseArmCommand(ArmSubsystem armSubsystem) {
        m_armSubsystem = armSubsystem;

        addRequirements(m_armSubsystem);
    }

    @Override
    public void initialize() {
        m_armSubsystem.raiseArm();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
