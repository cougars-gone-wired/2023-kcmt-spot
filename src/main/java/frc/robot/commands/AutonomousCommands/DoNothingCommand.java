package frc.robot.commands.AutonomousCommands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;

public class DoNothingCommand extends SequentialCommandGroup {
    public DoNothingCommand() {
        addCommands(
            new WaitCommand(0)
        );
    
    }
}
