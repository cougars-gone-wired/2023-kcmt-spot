// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import frc.robot.Constants;
import com.swervedrivespecialties.swervelib.MkSwerveModuleBuilder;
import com.swervedrivespecialties.swervelib.MotorType;
import com.swervedrivespecialties.swervelib.SdsModuleConfigurations;
import com.swervedrivespecialties.swervelib.SwerveModule;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveDriveOdometry;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import java.util.function.Consumer;
import java.util.function.Supplier;

import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.SPI;

public class DrivetrainSubsystem extends SubsystemBase {
    /**
     * The maximum voltage that will be delivered to the drive motors.
     * <p>
     * This can be reduced to cap the robot's maximum speed. Typically, this is
     * useful during initial testing of the robot.
     */
    public static final double MAX_VOLTAGE = 12.0;
    // The formula for calculating the theoretical maximum velocity is:
    // <Motor free speed RPM> / 60 * <Drive reduction> * <Wheel diameter meters> *
    // pi
    // By default this value is setup for a Mk3 standard module using Falcon500s to
    // drive.
    // An example of this constant for a Mk4 L2 module with NEOs to drive is:
    // 5880.0 / 60.0 / SdsModuleConfigurations.MK4_L2.getDriveReduction() *
    // SdsModuleConfigurations.MK4_L2.getWheelDiameter() * Math.PI

    /**
     * The maximum velocity of the robot in meters per second.
     * <p>
     * This is a measure of how fast the robot should be able to drive in a straight
     * line.
     */
    public static final double MAX_VELOCITY_METERS_PER_SECOND = 6380.0 / 60.0 *
            SdsModuleConfigurations.MK4I_L3.getDriveReduction() *
            SdsModuleConfigurations.MK4I_L3.getWheelDiameter() * Math.PI;
    /**
     * The maximum angular velocity of the robot in radians per second.
     * <p>
     * This is a measure of how fast the robot can rotate in place.
     */
    // Here we calculate the theoretical maximum angular velocity. You can also
    // replace this with a measured amount.
    public static final double MAX_ANGULAR_VELOCITY_RADIANS_PER_SECOND = MAX_VELOCITY_METERS_PER_SECOND /
            Math.hypot(Constants.SwerveConstants.DRIVETRAIN_TRACKWIDTH_METERS / 2.0,
                    Constants.SwerveConstants.DRIVETRAIN_WHEELBASE_METERS / 2.0);

    private SwerveModule m_frontLeftModule;
    private SwerveModule m_frontRightModule;
    private SwerveModule m_backLeftModule;
    private SwerveModule m_backRightModule;

    // By default we use a Pigeon for our gyroscope. But if you use another
    // gyroscope, like a NavX, you can change this.
    // The important thing about how you configure your gyroscope is that rotating
    // the robot counter-clockwise should
    // cause the angle reading to increase until it wraps back over to zero.
    private final AHRS m_navx = new AHRS(SPI.Port.kMXP, (byte) 200); // NavX connected over MXP

    private float m_pitchOffset = 0;
    private float m_rollOffset = 0;

    // The locations for the modules must be relative to the center of the robot. 
    // Positive x values represent moving toward the front of the robot.
    // Positive y values represent moving toward the left of the robot.
    public final SwerveDriveKinematics m_kinematics = new SwerveDriveKinematics(
        // front left (+/+)
        new Translation2d(
            Constants.SwerveConstants.DRIVETRAIN_WHEELBASE_METERS / 2.0,
            Constants.SwerveConstants.DRIVETRAIN_TRACKWIDTH_METERS / 2.0
        ),
        // front right (+/-)
        new Translation2d(
            Constants.SwerveConstants.DRIVETRAIN_WHEELBASE_METERS / 2.0,
            -Constants.SwerveConstants.DRIVETRAIN_TRACKWIDTH_METERS / 2.0
        ),
        // back left (-/+)
        new Translation2d(
            -Constants.SwerveConstants.DRIVETRAIN_WHEELBASE_METERS / 2.0,
            Constants.SwerveConstants.DRIVETRAIN_TRACKWIDTH_METERS / 2.0
        ),
        // back right (-/-)
        new Translation2d(
            -Constants.SwerveConstants.DRIVETRAIN_WHEELBASE_METERS / 2.0,
            -Constants.SwerveConstants.DRIVETRAIN_TRACKWIDTH_METERS / 2.0
        )
    );

    private SwerveDriveOdometry m_odometry = null;

    private ChassisSpeeds m_chassisSpeeds = new ChassisSpeeds(0.0, 0.0, 0.0);

    /**
     * Constructor.
     */
    public DrivetrainSubsystem() {
        ShuffleboardTab m_shuffleboardTab = Shuffleboard.getTab("Drivetrain");

        m_frontLeftModule = new MkSwerveModuleBuilder()
            .withLayout(m_shuffleboardTab.getLayout("Front Left Module", BuiltInLayouts.kList)
            .withSize(2, 4)
            .withPosition(0, 0))
            .withGearRatio(SdsModuleConfigurations.MK4I_L3)
            .withDriveMotor(MotorType.FALCON,
                Constants.SwerveConstants.FRONT_LEFT_MODULE_DRIVE_MOTOR)
            .withSteerMotor(MotorType.FALCON,
                Constants.SwerveConstants.FRONT_LEFT_MODULE_STEER_MOTOR)
            .withSteerEncoderPort(Constants.SwerveConstants.FRONT_LEFT_MODULE_STEER_ENCODER)
            .withSteerOffset(Constants.SwerveConstants.FRONT_LEFT_MODULE_STEER_OFFSET)
            .build();

        m_frontRightModule = new MkSwerveModuleBuilder()
            .withLayout(m_shuffleboardTab.getLayout("Front Right Module", BuiltInLayouts.kList)
            .withSize(2, 4)
            .withPosition(2, 0))
            .withGearRatio(SdsModuleConfigurations.MK4I_L3)
            .withDriveMotor(MotorType.FALCON,
                Constants.SwerveConstants.FRONT_RIGHT_MODULE_DRIVE_MOTOR)
            .withSteerMotor(MotorType.FALCON,
                Constants.SwerveConstants.FRONT_RIGHT_MODULE_STEER_MOTOR)
            .withSteerEncoderPort(Constants.SwerveConstants.FRONT_RIGHT_MODULE_STEER_ENCODER)
            .withSteerOffset(Constants.SwerveConstants.FRONT_RIGHT_MODULE_STEER_OFFSET)
            .build();

        m_backLeftModule = new MkSwerveModuleBuilder()
            .withLayout(m_shuffleboardTab.getLayout("Back Left Module", BuiltInLayouts.kList)
            .withSize(2, 4)
            .withPosition(4, 0))
            .withGearRatio(SdsModuleConfigurations.MK4I_L3)
            .withDriveMotor(MotorType.FALCON,
                Constants.SwerveConstants.BACK_LEFT_MODULE_DRIVE_MOTOR)
            .withSteerMotor(MotorType.FALCON,
                Constants.SwerveConstants.BACK_LEFT_MODULE_STEER_MOTOR)
            .withSteerEncoderPort(Constants.SwerveConstants.BACK_LEFT_MODULE_STEER_ENCODER)
            .withSteerOffset(Constants.SwerveConstants.BACK_LEFT_MODULE_STEER_OFFSET)
            .build();

        m_backRightModule = new MkSwerveModuleBuilder()
            .withLayout(m_shuffleboardTab.getLayout("Back Right Module", BuiltInLayouts.kList)
            .withSize(2, 4)
            .withPosition(6, 0))
            .withGearRatio(SdsModuleConfigurations.MK4I_L3)
            .withDriveMotor(MotorType.FALCON,
                Constants.SwerveConstants.BACK_RIGHT_MODULE_DRIVE_MOTOR)
            .withSteerMotor(MotorType.FALCON,
                Constants.SwerveConstants.BACK_RIGHT_MODULE_STEER_MOTOR)
            .withSteerEncoderPort(Constants.SwerveConstants.BACK_RIGHT_MODULE_STEER_ENCODER)
            .withSteerOffset(Constants.SwerveConstants.BACK_RIGHT_MODULE_STEER_OFFSET)
            .build();

        m_odometry = new SwerveDriveOdometry(
            m_kinematics,
            this._getGyroscopeRotation(),
            new SwerveModulePosition[] { 
                m_frontLeftModule.getPosition(),
                m_frontRightModule.getPosition(), 
                m_backLeftModule.getPosition(),
                m_backRightModule.getPosition() 
            }
        );

        m_shuffleboardTab.addNumber("Gyroscope Angle", () -> _getGyroscopeRotation().getDegrees());
        m_shuffleboardTab.addNumber("Rotation", () -> getRotation().getDegrees());
        m_shuffleboardTab.addNumber("Pose X (B<->F)", () -> m_odometry.getPoseMeters().getX());
        m_shuffleboardTab.addNumber("Pose Y (R<->L)", () -> m_odometry.getPoseMeters().getY());
    }

    /**
     * Sets the odometry offset to the current gyro angle. 
     * This can be used to set the direction the robot is 
     * currently facing to the new 'forwards' direction (0 degrees).
     */
    public void zeroGyroscope() {
        m_odometry.resetPosition(
            _getGyroscopeRotation(),
            new SwerveModulePosition[]{ 
                m_frontLeftModule.getPosition(), 
                m_frontRightModule.getPosition(),
                m_backLeftModule.getPosition(), 
                m_backRightModule.getPosition() 
            },
            new Pose2d(m_odometry.getPoseMeters().getTranslation(), Rotation2d.fromDegrees(0.0))
        );
    }

    /**
     * Current odometry rotation in relation to the latest reset.
     * @return {Rotation2d} Odometry rotation.  
     */
    public Rotation2d getRotation() {
        return m_odometry.getPoseMeters().getRotation();
    }

    /**
     * Return the yaw angle (0 - 360 counter clockwise)
     * @return {Rotation2d} Current yaw angle
     */
    private Rotation2d _getGyroscopeRotation() {
        // System.out.println("Not Mag");
        float yaw = m_navx.getYaw();
        // convert from -180 - 180 (clockwise) to 0 - 360 (counter clockwise)
        return Rotation2d.fromDegrees( 360 - (yaw < 0 ? yaw + 360 : yaw));
    }

    /**
     * Used by Path Planner to read current odemetry pose.
     * @returns {Pose2d} Current robot position as reported by odometry.
     */
    public Supplier<Pose2d> getPose = () -> m_odometry.getPoseMeters();

    /**
     * Used by Path Planner to set robot starting pose.
     * @param {Pose2d} Position to set.
     */
    public Consumer<Pose2d> resetPose = (pose) -> {
        m_odometry.resetPosition(
            _getGyroscopeRotation(),
            new SwerveModulePosition[] {
                m_frontLeftModule.getPosition(),
                m_frontRightModule.getPosition(),
                m_backLeftModule.getPosition(),
                m_backRightModule.getPosition()
            },
            pose
        );
    };

    /**
     * Used by Path Planner to set each swerve module's speed and angle.
     * @param {SwerveModuleState[]} states Array of modue states to set.
     */
    public Consumer<SwerveModuleState[]> setModuleStates = (states) -> {
        m_chassisSpeeds = m_kinematics.toChassisSpeeds(states);
    };

    /**
     * Called to set module speeds to set on the next periodic call.
     * @param {ChassisSpeeds} chassisSpeeds 
     */
    public void drive(ChassisSpeeds chassisSpeeds) {
        m_chassisSpeeds = chassisSpeeds;
    }

    /**
     * Start NAVx calibration.
     */
    public void recal() {
        m_navx.calibrate();
    }

    /**
     * Return NAVx calibration state.
     * @return {boolean} True = calibrating.
     */
    public boolean calibrating() {
        return m_navx.isCalibrating();
    }

    /**
     * Read current gyro pitch/roll values and save as current 'up'.
     */
    public void setOffsets() {
        m_pitchOffset = _getGyroPitch();
        m_rollOffset = _getGyroRoll();
    }

    /**
     * Return pitch (nose up).
     * @return Pitch in degrees.
     */
    private float _getGyroPitch() {
        // NavX 2 switched pitch and roll
        return m_navx.getRoll();
    }

    /**
     * Return roll (left up).
     * @return Roll in degrees.
     */
    private float _getGyroRoll() {
        // NavX 2 switched pitch and roll
        return m_navx.getPitch();
    }

    /**
     * Return corrected pitch angle (nose up).
     * @return Pitch in degrees.
     */
    public float getPitch() {
        return ((int) ((_getGyroPitch() - m_pitchOffset) * 100)) / 100f;
    }

    /**
     * Return correctedroll (left up).
     * @return Roll in degrees.
     */
    public float getRoll() {
        return ((int) ((_getGyroRoll() - m_rollOffset) * 100)) / 100f;
    }

    /**
     * Subsystem periodic function.
     * 1. Update odometry to current module positions.
     * 2. Update kinematics to chassis speeds set by the drive() function
     */
    @Override
    public void periodic() {
        SmartDashboard.putNumber("Fused Heading: ", m_navx.getFusedHeading());
        SmartDashboard.putNumber("NavX get yaw: ", m_navx.getYaw());
        SmartDashboard.putNumber("Odometry Rot: ", m_odometry.getPoseMeters().getRotation().getDegrees());

        m_odometry.update(
            _getGyroscopeRotation(),
            new SwerveModulePosition[] { 
                m_frontLeftModule.getPosition(),
                m_frontRightModule.getPosition(), 
                m_backLeftModule.getPosition(),
                m_backRightModule.getPosition() 
            }
        );

        SwerveModuleState[] states = m_kinematics.toSwerveModuleStates(m_chassisSpeeds);
        m_kinematics.desaturateWheelSpeeds(states, MAX_VELOCITY_METERS_PER_SECOND);
        m_frontLeftModule.set(
            states[0].speedMetersPerSecond / MAX_VELOCITY_METERS_PER_SECOND * MAX_VOLTAGE,
            states[0].angle.getRadians()
        );
        m_frontRightModule.set(
            states[1].speedMetersPerSecond / MAX_VELOCITY_METERS_PER_SECOND * MAX_VOLTAGE,
            states[1].angle.getRadians()
        );
        m_backLeftModule.set(
            states[2].speedMetersPerSecond / MAX_VELOCITY_METERS_PER_SECOND * MAX_VOLTAGE,
            states[2].angle.getRadians()
        );
        m_backRightModule.set(
            states[3].speedMetersPerSecond / MAX_VELOCITY_METERS_PER_SECOND * MAX_VOLTAGE,
            states[3].angle.getRadians()
        );
    }
}