package frc.robot.subsystems;

import edu.wpi.first.wpilibj.PneumaticsControlModule;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ArmSubsystem extends SubsystemBase {
    private Solenoid m_leftSolenoid;
    private Solenoid m_rightSolenoid;
    private PneumaticsControlModule m_pcm;
    private boolean m_compressorEnabled = true;
    private int m_actuationCount;

    public ArmSubsystem() {
        m_actuationCount = 0;
        m_pcm = new PneumaticsControlModule();
        m_leftSolenoid = new Solenoid(PneumaticsModuleType.CTREPCM, Constants.ArmConstants.LEFT_SOLENOID_ID);
        m_rightSolenoid = new Solenoid(PneumaticsModuleType.CTREPCM, Constants.ArmConstants.RIGHT_SOLENOID_ID);
    }

    public void raiseArm() {
        m_leftSolenoid.set(true);
        m_rightSolenoid.set(true);
        m_actuationCount++;
    }

    public void lowerArm() {
        m_leftSolenoid.set(false);
        m_rightSolenoid.set(false);
        m_actuationCount++;
    }

    public void disableCompressor() {
        m_pcm.disableCompressor();
        m_compressorEnabled = false;
    }

    public void enableCompressor() {
        m_pcm.enableCompressorDigital();
        m_compressorEnabled = true;
    }

    public boolean getArmRaised() {
        return m_leftSolenoid.get() && m_rightSolenoid.get();
    }

    @Override
    public void periodic() {
        SmartDashboard.putBoolean("Compressor Enabled", m_compressorEnabled);
        SmartDashboard.putNumber("Actuation Count", m_actuationCount);
    }
}
