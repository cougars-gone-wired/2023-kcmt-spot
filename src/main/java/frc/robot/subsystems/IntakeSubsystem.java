package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class IntakeSubsystem extends SubsystemBase {
    private WPI_TalonSRX m_rollerMotor;

    public IntakeSubsystem() {
        m_rollerMotor = new WPI_TalonSRX(Constants.IntakeConstants.ROLLER_MOTOR_ID);
    }

    public void rollIn() {
        m_rollerMotor.set(Constants.IntakeConstants.ROLLER_SPEED);
    }

    public void rollOut() {
        m_rollerMotor.set(-Constants.IntakeConstants.ROLLER_SPEED);
    }

    public void stopRoller() {
        m_rollerMotor.set(0);
    }

    public void rollSpeed(double speed) {
        m_rollerMotor.set(speed * Constants.IntakeConstants.ROLLER_SPEED);
    }
}
